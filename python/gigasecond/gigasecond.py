from datetime import timedelta
def add(moment: classmethod) -> classmethod:
    return moment + timedelta(seconds=10**9)